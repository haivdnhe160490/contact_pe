package com.example.contact;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.FirebaseDatabase;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainAdapter extends FirebaseRecyclerAdapter<Contact, MainAdapter.myViewHolder> {
    /**
     * Initialize a {@link RecyclerView.Adapter} that listens to a Firebase query. See
     * {@link FirebaseRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public MainAdapter(@NonNull FirebaseRecyclerOptions<Contact> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull myViewHolder holder, final int position, @NonNull Contact model) {
        holder.id.setText(model.id);
        holder.name.setText(model.name);
        holder.email.setText(model.email);
        holder.address.setText(model.address);
        holder.company.setText(model.company);

        Glide.with(holder.img.getContext())
                .load(model.photo)
                .placeholder(com.google.firebase.database.R.drawable.common_google_signin_btn_icon_dark)
                .circleCrop()
                .error(com.google.firebase.database.R.drawable.common_google_signin_btn_icon_dark_normal)
                .into(holder.img);
        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DialogPlus dialogPlus = DialogPlus.newDialog(holder.img.getContext())
                        .setContentHolder(new ViewHolder(R.layout.update_popup))
                        .setExpanded(true,1700).create();

                View view =dialogPlus.getHolderView();

                EditText name = view.findViewById(R.id.txtName);
                EditText email = view.findViewById(R.id.txtEmail);
                EditText company = view.findViewById(R.id.txtCompany);
                EditText address = view.findViewById(R.id.txtAddress);
                EditText photo = view.findViewById(R.id.txtPhoto);

                Button btnUpdate = view.findViewById(R.id.btnUpdate);
                name.setText(model.getName());
                email.setText(model.getEmail());
                company.setText(model.getCompany());
                address.setText(model.getAddress());
                photo.setText(model.getPhoto());

                dialogPlus.show();

                btnUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Map<String,Object> map = new HashMap<>();
                        map.put("name",name.getText().toString());
                        map.put("email",email.getText().toString());
                        map.put("company",company.getText().toString());
                        map.put("address",address.getText().toString());
                        map.put("photo",photo.getText().toString());

                        FirebaseDatabase.getInstance("https://contact-fd585-default-rtdb.asia-southeast1.firebasedatabase.app")
                                .getReference().child("Contact").child(getRef(position).getKey()).updateChildren(map)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void unused) {
                                        Toast.makeText(holder.name.getContext(), "Update Success", Toast.LENGTH_SHORT).show();
                                        dialogPlus.dismiss();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {
                                        Toast.makeText(holder.name.getContext(), "Update Failed", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                });

            }
        });
        holder.btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(holder.name.getContext());
                builder.setTitle("Are you Sure?");
                builder.setMessage("Delete data can not be Undo!");

                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseDatabase.getInstance("https://contact-fd585-default-rtdb.asia-southeast1.firebasedatabase.app")
                                .getReference().child("Contact").child(getRef(position).getKey()).removeValue();
                    }
                });
                builder.setNegativeButton("Cancle", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(holder.name.getContext(), "Cancelled", Toast.LENGTH_SHORT).show();
                    }
                });
                builder.show();
            }
        });
    }

    @NonNull
    @Override
    public myViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_item, parent,false);
        return new myViewHolder(view);
    }

    class myViewHolder extends RecyclerView.ViewHolder{
        CircleImageView img;
        TextView id,name,email,company,address,photo;

         Button btnEdit, btnDel;
        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            img = (CircleImageView)itemView.findViewById(R.id.img1);
            id = (TextView)itemView.findViewById(R.id.idtxt);
            name = (TextView)itemView.findViewById(R.id.nametxt);
            email = (TextView)itemView.findViewById(R.id.emailtxt);
            company = (TextView)itemView.findViewById(R.id.companytxt);
            address = (TextView)itemView.findViewById(R.id.addresstxt);

            btnEdit=(Button)itemView.findViewById(R.id.btnEdit);
            btnDel=(Button)itemView.findViewById(R.id.btnDelete);
        }
    }
}
