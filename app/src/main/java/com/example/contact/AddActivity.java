package com.example.contact;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class AddActivity extends AppCompatActivity {
    EditText name,email,company,address,photo;
    Button btnAdd, btnBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        name = (EditText)findViewById(R.id.txtName);
        email = (EditText)findViewById(R.id.txtEmail);
        company = (EditText)findViewById(R.id.txtCompany);
        address = (EditText)findViewById(R.id.txtAddress);
        photo = (EditText)findViewById(R.id.txtPhoto);

        btnAdd = (Button)findViewById(R.id.btnAdd);
        btnBack = (Button)findViewById(R.id.btnBack);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertData();
            }
        });
    }

    private void insertData(){
        Map<String,Object> map = new HashMap<>();
        map.put("id", generateRandomID());
        map.put("name", name.getText().toString());
        map.put("email", email.getText().toString());
        map.put("company", company.getText().toString());
        map.put("address", address.getText().toString());
        map.put("photo", photo.getText().toString());

        FirebaseDatabase.getInstance("https://contact-fd585-default-rtdb.asia-southeast1.firebasedatabase.app")
                .getReference().child("Contact").push()
                .setValue(map).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        Toast.makeText(AddActivity.this, "Save Success", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(AddActivity.this, "Save Failed", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private static String generateRandomID() {
        String letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Random random = new Random();
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < 2; i++) {
            int randomIndex = random.nextInt(letters.length());
            builder.append(letters.charAt(randomIndex));
        }

        long timestamp = System.currentTimeMillis();
        builder.append(timestamp);

        return builder.toString();
    }
}